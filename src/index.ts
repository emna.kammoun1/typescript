console.log('hello world')
let age : number =20
if (age < 50) 
    age +=10
//console.log(age)

let sales : number = 123_456_789
//let sales = 123_456_789 
let course : string = 'typeScript'
//let course = 'typeScript'
let is_published : boolean = true
// let is_published = true
 let level //de type any

 //any 
 function render(document : any) {
    console.log(document)
 }

//arrays
let numbers : number[] = [1 , 2 , 3] //de type number

//ou bien
let number = [111,22,33]

let tab = []
tab[0] =1
tab[1] ='2'

let tabs : number[] = []
tabs.forEach(n => n.toExponential)

//tuple 
let user : [number,string] = [1,'emna']
user[1].lastIndexOf
user.push(1)
console.log(user)

let heelo : [number , string , boolean , number] = [ 1 ,'emna' ,true , 0]

//enums : list of constante
const small =1
const medium =2
const large = 3
enum Size {Small , Medium , Large} // {0,1,2}
enum Aa {Small ='s', Medium ='m', Large='l'} // { Small: 's', Medium: 'm', Large: 'l' }
const enum Bb {Small = 1, Medium, Large} //yebda mel 1 sinon ki manhotech yebda m 0
let mySize: Bb =Bb.Medium
console.log(mySize) //2
console.log(Size)
console.log(Aa)

//function
function calculateTax (income : number ,taxYear=2022) :number{ //default value
    if (taxYear < 2020)
        return income*1.2
        //undifined
    return income *1.3
}

calculateTax(10_000)

//objects
let employee : {
  readonly id :number ,
    name:string
    retire :(date:Date) => void
} = { 
    id :1 ,
     name :'emna',
     retire :(date :Date) => {
        console.log(date)
     }

    } 
    console.log(employee)   
//employee.id =0
//{ id: 1, name: 'emna', retire: [Function: retire] }


//type alias
type Employees = {
    readonly id :number ,
    name:string
    retire :(date:Date) => void
}
let employees :Employees= {
      id :2 ,
       name :'emna',
       retire :(date :Date) => {
          console.log(date)
       }
  
      }
      
 
  //union type
  function kgToLbs (weights : number | string) {
        //Narrowing
    if(typeof weights ==='number') 
        return weights*1.2
    else
        return parseInt(weights) *2.2
  }
  console.log(kgToLbs(20))
console.log(  kgToLbs('20kg'))

//interrection type*

type Draggable ={
    drag : () => void
}
type Resizable = {
    resize : () => void
} 
type UIWidget = Draggable & Resizable
let textBox: UIWidget ={
    drag:() => {} ,
    resize: () => {}
}

//Literal type to limit the number 
type Quantity = 50 | 100 
let quantity : Quantity = 100

type Metric ='cm' | 'inch' 
let metric :Metric = 'cm'

//Nullable type
function greet (name : string | null | undefined) {
    if (name)
         console.log(name.toUpperCase())
    else 
        console.log('hola')
}
greet(null)

//optional chaining
 type Customer = {
    birthday? : Date 
 }
 function getCustemer (id : number ) :Customer | null  | undefined{
    return id=== 0 ? null : { birthday : new Date()}
 }
let customer = getCustemer(1)
console.log(customer)
//if (customer!== null && customer!== undefined)
// ou utiliser optional proprety accecc operator
console.log(customer?.birthday?.getFullYear())


//optional element access operator
//console.log(customer?.[0])
let log: any = null
console.log(log?.('a'))
