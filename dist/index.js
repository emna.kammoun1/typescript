"use strict";
var _a;
console.log('hello world');
let age = 20;
if (age < 50)
    age += 10;
let sales = 123456789;
let course = 'typeScript';
let is_published = true;
let level;
function render(document) {
    console.log(document);
}
let numbers = [1, 2, 3];
let number = [111, 22, 33];
let tab = [];
tab[0] = 1;
tab[1] = '2';
let tabs = [];
tabs.forEach(n => n.toExponential);
let user = [1, 'emna'];
user[1].lastIndexOf;
user.push(1);
console.log(user);
let heelo = [1, 'emna', true, 0];
const small = 1;
const medium = 2;
const large = 3;
var Size;
(function (Size) {
    Size[Size["Small"] = 0] = "Small";
    Size[Size["Medium"] = 1] = "Medium";
    Size[Size["Large"] = 2] = "Large";
})(Size || (Size = {}));
var Aa;
(function (Aa) {
    Aa["Small"] = "s";
    Aa["Medium"] = "m";
    Aa["Large"] = "l";
})(Aa || (Aa = {}));
let mySize = 2;
console.log(mySize);
console.log(Size);
console.log(Aa);
function calculateTax(income, taxYear = 2022) {
    if (taxYear < 2020)
        return income * 1.2;
    return income * 1.3;
}
calculateTax(10000);
let employee = {
    id: 1,
    name: 'emna',
    retire: (date) => {
        console.log(date);
    }
};
console.log(employee);
let employees = {
    id: 2,
    name: 'imen',
    retire: (date) => {
        console.log(date);
    }
};
function kgToLbs(weights) {
    if (typeof weights === 'number')
        return weights * 1.2;
    else
        return parseInt(weights) * 2.2;
}
console.log(kgToLbs(20));
console.log(kgToLbs('20kg'));
let textBox = {
    drag: () => { },
    resize: () => { }
};
let quantity = 100;
let metric = 'cm';
function greet(name) {
    if (name)
        console.log(name.toUpperCase());
    else
        console.log('hola');
}
greet(null);
function getCustemer(id) {
    return id === 0 ? null : { birthday: new Date() };
}
let customer = getCustemer(1);
console.log(customer);
console.log((_a = customer === null || customer === void 0 ? void 0 : customer.birthday) === null || _a === void 0 ? void 0 : _a.getFullYear());
let log = null;
console.log(log === null || log === void 0 ? void 0 : log('a'));
